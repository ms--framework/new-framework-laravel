<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MS-Framework-Bakcend-Laravel</title>
</head>

<body>
    <h1>Welcome sur notre Backend.</h1>
    <div>
        LARAVEL v<?php echo e(Illuminate\Foundation\Application::VERSION); ?> - PHP v<?php echo e(PHP_VERSION); ?> - APP
        v<?php echo e(env('APP_VERSION')); ?>

    </div>
</body>

</html>
<?php /**PATH D:\Projects\Webtinix\Projets\M. Ovage Cyriaque\sample-project\api-laravel\resources\views/welcome.blade.php ENDPATH**/ ?>