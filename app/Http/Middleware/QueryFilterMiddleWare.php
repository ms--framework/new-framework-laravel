<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QueryFilterMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if($request->hasHeader('Authorization')){
            $jwt_token = $request->header("Authorization");
            $jwt_tokens = explode(" ", $jwt_token);
            $jwt_token = end($jwt_tokens);

            $user = auth()->setToken($jwt_token)->user();

            if($user !== null){
                return $next($request);
            }
        }

        return response()->json(["message"=>"Authentication requise"], 401);
    }
}
