<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class StoreCommandeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            //

            'produits' => [
                'required',
                'array'
            ],

            'produits.*.produit_id' => [
                'required',
                'exists:produits,id'
            ],

            'produits.*.quantite' => [
                'required',
                'integer'
            ],

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [

            'produits.required' => 'Vous devez sélectionner au moins un produit',
            'produits.array' => 'Vous devez sélectionner au moins un produit',


            'produits.*.produit_id.required' => 'Le produit est requis',
            'produits.*.produit_id.exists' => 'Le produit n\'existe pas',


            'produits.*.quantite.required' => 'La quantité est requise',
            'produits.*.quantite.integer' => 'La quantité doit être un entier',

        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'message' => 'Erreur lors de la creation de la commande',
            'data' => $validator->errors()
        ], 422));
    }
}
