<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;


class StoreProduitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            //

            'libelle' => [
                'required',
                Rule::unique('produits', 'libelle'),
                'string',
                'max:255',
            ],

            'desc' => [
                'required',
                'string',
            ],

            'prix' => [
                'required',
                'integer',
                'numeric',
            ],

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [

            'libelle.required' => 'Le libellé du produit est requis',
            'libelle.unique' => 'Le libellé du produit doit être unique',
            'libelle.string' => 'Le libellé du produit doit être valide',
            'libelle.max' => 'Le libellé du produit doit être inférieur à 255 caractères',

            'desc.required' => 'La description est requise',
            'desc.string' => 'La description doit être valide',

            'prix.required' => 'Le prix est requis',
            'prix.integer' => 'Le prix doit être entier',
            'prix.numeric' => 'Le prix doit être valide',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'message' => 'Erreur lors de la creation du produit',
            'data' => $validator->errors()
        ], 422));
    }

}
