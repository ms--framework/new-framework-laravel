<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use App\Http\Requests\StoreCommandeRequest;
use App\Http\Requests\UpdateCommandeRequest;
use App\Http\Resources\CommandeResource;
use App\Models\Produit;
use App\Models\ProduitCommande;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class CommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return CommandeResource::collection(Commande::paginate()->withQueryString())
                    ->response()
                    ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCommandeRequest $request)
    {
        //
        $validated = $request->validated();

        $produits = $validated['produits'];

        $commande = new Commande();
        $uuid4s = explode('-', Uuid::uuid4()->toString());
        $commande->numero = end($uuid4s);
        $commande->user()->associate(auth()->user());
        $commande->save();

        $prods = collect($produits)->keyBy('produit_id')->all();
        $commande->produits()->attach($prods);

        /* foreach ($produits as $produit) {
            $commande->produits()->attach($produit['produit_id'], ['quantite' => $produit['quantite']]);
        } */

        return (new CommandeResource($commande->loadMissing(['user', 'produits'])))
                ->response()
                ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Commande $commande)
    {
        //
        return (new CommandeResource($commande->loadMissing(['user', 'produits'])))
                ->response()
                ->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCommandeRequest $request, Commande $commande)
    {
        //
        $validated = $request->validated();

        $produits = $validated['produits'];

        $prods = collect($produits)->keyBy('produit_id')->all();
        $commande->produits()->sync($prods);

        return (new CommandeResource($commande->loadMissing(['user', 'produits'])))
                ->response()
                ->setStatusCode(201);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Commande $commande)
    {
        //
        $commande->produits()->detach();
        $commande->delete();

        return response()->json(["message" => "Commande supprimée avec succès"], 201);
    }
}
