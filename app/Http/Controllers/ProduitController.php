<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use App\Http\Requests\StoreProduitRequest;
use App\Http\Requests\UpdateProduitRequest;
use App\Http\Resources\ProduitResource;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return ProduitResource::collection(Produit::paginate()->withQueryString())
                    ->response()
                    ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProduitRequest $request)
    {
        //
        $validated = $request->validated();

        $produit = Produit::create($validated);

        return (new ProduitResource($produit))
                    ->response()
                    ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Produit $produit)
    {
        //
        return (new ProduitResource($produit))
                    ->response()
                    ->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProduitRequest $request, Produit $produit)
    {
        //
        $validated = $request->validated();

        $produit->update($validated);

        $produit->refresh();

        return (new ProduitResource($produit))
                ->response()
                ->setStatusCode(201);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Produit $produit)
    {
        //
        $produit->delete();

        return response()->json(["message" => "Produit supprimé avec succès"], 201);
    }
}
