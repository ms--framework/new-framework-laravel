<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'libelle',
        'desc',
        'prix',
    ];

    public function commandes()
    {
        // return $this->belongsToMany(Commande::class, 'produit_commandes');
        return $this->belongsToMany(Commande::class, 'produit_commandes')->using(ProduitCommande::class);
    }


}
