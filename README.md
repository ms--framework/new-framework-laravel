<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains thousands of video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Installation

Avec Composer

```
composer create-project laravel/laravel example-app
```

Ou vous pouvez également créer de nouveaux projets Laravel en installant globalement le programme d'installation de Laravel via Composer :

```
composer global require laravel/installer
 
laravel new example-app
```

## Tester

```
cd example-app
 
php artisan serve
```

## Installation Sanctum

```
php artisan install:api
```

## Installation JWT

Référence de documentation JWT pour Laravel :

- [Install JWT in Laravel app — 2023 Guide for beginners](https://medium.com/@demian.kostelny/install-jwt-in-laravel-app-2023-guide-for-beginners-94f7245ce6bd).
- [Laravel Installation - jwt-auth - Read the Docs](https://jwt-auth.readthedocs.io/en/develop/).

```
composer require tymon/jwt-auth
```

Ajout d'un fournisseur de service JWT dans le tableau "providers" du ficher "config/app.php" ( Pour Laravel 5.4 ou inférieur ):

```php
<?php
    // config/app.php

    'providers' => [
        // ...
        Tymon\JWTAuth\Providers\LaravelServiceProvider::class,
    ]
```

Publier le fichier de configuration du fournisseur de service pour JWT :

```
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
```

Générer une clé secrète pour JWT : 

```
php artisan jwt:secret
```

Mettez à jour votre modèle d'utilisateur (app/Models/User.php) :

```php
<?php
// app/Models/User.php
namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
```

Configuration de l'Auth Guard (Laravel 5.2 ou supérieur)

```php
<?php
// config/auth.php

'defaults' => [
    'guard' => 'api',
    'passwords' => 'users',
],

...

'guards' => [
    'api' => [
        'driver' => 'jwt',
        'provider' => 'users',
    ],
],
```

A partir de Laravel 11

```php
<?php
// config/auth.php

...

'guards' => [
    'api' => [
        'driver' => 'jwt',
        'provider' => 'users',
    ],
],
```

```bash
# .env

...

AUTH_GUARD=api
AUTH_PASSWORD_BROKER=users
```

On indique à l'api guard d'utiliser le pilote jwt, et on définit l'api guard par défaut.


Ajout des routes d'authentification de base :

```php
<?php
//routes/api.php

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});
```

Créer le contrôleur d'authentification (AuthController) :

```bash
php artisan make:controller AuthController
```

Mise à jour du contrôleur d'authentification (AuthController) :

```php
<?php
// app/Http/Controllers/AuthController.php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
```

Vous devriez maintenant être en mesure de POST au point de terminaison de connexion (par exemple, http://example.dev/auth/login) avec des informations d'identification valides et de voir une réponse comme :

```json
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
    "token_type": "bearer",
    "expires_in": 3600
}
```

Ce jeton peut ensuite être utilisé pour effectuer des requêtes authentifiées auprès de votre application.

Requêtes authentifiées :

En-tête d'autorisation

```http
Authorization: Bearer eyJhbGciOiJIUzI1NiI...
```

ou

Paramètre de la chaîne de requête (Query string)

```http
http://example.dev/me?token=eyJhbGciOiJIUzI1NiI...
```

Création d'un middleware pour filtrer les requêtes

```bash
php artisan make:middleware QueryFilterMiddleWare
```

Edition du fichier du Middleware

```php
<?php
// app/Http/Middleware/QueryFilterMiddleWare.php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QueryFilterMiddleWare
{
    // ...
    public function handle(Request $request, Closure $next): Response
    {
        // Middleware body code

        return $next($request);
    }
}
```

Utilisation du middleware 

```php
<?php
// routes/api.php

Route::middleware([QueryFilterMiddleWare::class])->->group(function() {

    // ...
});

```

Différentes possibilités de création d'un modèle :

```bash
# Generate a model and a FlightFactory class...
php artisan make:model Flight --factory
php artisan make:model Flight -f
 
# Generate a model and a FlightSeeder class...
php artisan make:model Flight --seed
php artisan make:model Flight -s
 
# Generate a model and a FlightController class...
php artisan make:model Flight --controller
php artisan make:model Flight -c
 
# Generate a model, FlightController resource class, and form request classes...
php artisan make:model Flight --controller --resource --requests
php artisan make:model Flight -crR
 
# Generate a model and a FlightPolicy class...
php artisan make:model Flight --policy
 
# Generate a model and a migration, factory, seeder, and controller...
php artisan make:model Flight -mfsc
 
# Shortcut to generate a model, migration, factory, seeder, policy, controller, and form requests...
php artisan make:model Flight --all
php artisan make:model Flight -a
 
# Generate a pivot model...
php artisan make:model Member --pivot
php artisan make:model Member -p
```

Création du modèle **Produit** :

```bash
php artisan make:model Produit --all
```

Ajout des champs au modèle **Produit** se fait dans le fichier de migration du modèle **Produit**:

```path
database/migrations/aaaa_mm_dd_ssssss_create_produits_table.php
```

Création manuelle d'une migration pour produit, par exemple pour modifier les champs 
d'une base de données en production :

```bash
php artisan make:migration update_produits_table
```

Création de ressources individuelles

```bash
php artisan make:resource UserResource
```

Création de collection de ressources

```bash
php artisan make:resource User --collection
 
php artisan make:resource UserCollection
```
