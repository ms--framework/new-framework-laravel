<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\VoitureController;
use App\Http\Middleware\QueryFilterMiddleWare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');


Route::prefix('v1')->group(function() {


    Route::group([

        'middleware' => 'api',
        'prefix' => 'auth'

    ], function () {

        Route::post('register', [AuthController::class, 'register']);
        Route::post('login', [AuthController::class, 'login']);
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('refresh', [AuthController::class, 'refresh']);
        Route::post('me', [AuthController::class, 'me']);

    });

    Route::apiResource('voitures', VoitureController::class)->missing(function ($request) {
        return response()->json(['message' => 'voitures introuvable'], 404);
    });

    Route::middleware([QueryFilterMiddleWare::class, 'auth'])->group(function () {

        # Users routes
        # Route::apiResource('users', UserController::class);

        # Produits routes
        Route::apiResource('produits', ProduitController::class)->missing(function ($request) {
            return response()->json(['message' => 'Produit introuvable'], 404);
        });

        # Commandes routes
        Route::apiResource('commandes', CommandeController::class)->missing(function ($request) {
            return response()->json(['message' => 'Commande introuvable'], 404);
        });

    });


});




