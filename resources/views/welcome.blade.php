<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MS-Framework-Bakcend-Laravel</title>
</head>

<body>
    <h1>Welcome sur notre Backend.</h1>
    <div>
        LARAVEL v{{ Illuminate\Foundation\Application::VERSION }} - PHP v{{ PHP_VERSION }} - APP
        v{{ env('APP_VERSION') }}
    </div>
</body>

</html>
